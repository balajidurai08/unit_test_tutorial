from ..source.users import create_user, create_user_with_email, get_user
from mock import patch



def test_user(mock_user):
    name = mock_user['user_name']
    age = mock_user['age']
    city = mock_user['city']
    result = create_user(name, age, city)
    assert mock_user == result


def test_user_with_email(mock_user):
    name = mock_user['user_name']
    age = mock_user['age']
    city = mock_user['city']
    mock_user['email'] = f'{name}@example.com'
    result = create_user_with_email(name, age, city)
    assert mock_user == result

@patch('requests.get', return_value={'status_code': 200})
def test_get_user(mock_request):
    resp = get_user()
    assert resp == {'status_code': 200}