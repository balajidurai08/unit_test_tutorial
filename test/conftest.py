import pytest


@pytest.fixture(scope='function')
def pi_value():
    return 3.14


@pytest.fixture(scope='function')
def mock_user():
    return {"user_name": "Jega", "age": 53, "city": "chennai"}