from ..source.calculator import add, sub, div, mul, pi


def test_success():
    assert True


def test_success_entry():
    print('testing success case')
    assert True


def test_add():
    result = add(2, 3)
    assert result == 5


def test_add_2():
    result = add('2.5', '3.2')
    assert result == 5.7


def test_pi(pi_value):
    assert pi() == pi_value