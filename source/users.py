import requests

def create_user(name, age, city):
    print(f'Creating user with {name}, {age}, {city}')
    return {"user_name": name, "age": age, "city": city}


def create_user_with_email(name, age, city):
    email = f'{name}@example.com'
    return {"user_name": name, "age": age, "city": city, "email": email}


def get_user():
    resp = requests.get('https://jsonplaceholder.typicode.com/todos/1')
    print(f'mock resp --> {resp}')
    return {'status_code': resp['status_code']}