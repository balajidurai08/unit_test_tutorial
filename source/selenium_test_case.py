from selenium import webdriver

def automate_ui_test():
    driver = webdriver.Chrome()
    driver.get('https://demoqa.com/text-box')
    k = driver.find_element_by_id('userName')
    
    driver.find_element_by_xpath('//button[@id="submit"]').click()

    t = driver.find_element_by_xpath('//p[@id="name"]').text
    driver.close()

    return t.split(':')[-1]
